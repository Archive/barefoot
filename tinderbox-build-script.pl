#!/usr/bin/perl -w

# TODO:
# separate package system independent stuff into a separate module/file
# separate config into a separate file
#
# Usage:
# ./tinderbox-build-script.pl [modulename] [cvsroot]
#
# Example:
# ./tinderbox-build-script.pl glib :pserver:garrison@cvs.gnome.org:/cvs/gnome

use strict;
use GnomeTinderboxBuild;
use IPC::Open3;

our $RPMDIR = '/home/garrison/rpm';
our $datestr = '204'; # FIXME: this should be the actual date
our $OS = "debian-sid";

our $modulename = shift;
our $cvsroot = shift;

$cvsroot ||= ':pserver:garrison@cvs.gnome.org:/cvs/gnome';
our $topsrcdir = '/tinderbox/src';
#our $topbuilddir = '/tinderbox/build';
our $modulesrcdir = $topsrcdir . '/' . $modulename;
our $modulebuilddir = $modulesrcdir;
#our $modulebuilddir = $topbuilddir . '/' . $modulename;

our ($packagename, $packageversion);
our $logmsg = '';

&common_prepare_code;
&prepare_package_system;
our $build = start GnomeTinderboxBuild "$packagename-$OS-$packageversion";
&configure_code;
&customize_package_system; # only part of this needs to be here
&prepare_code_archive;
&prepare_system;
&launch_package_build;
&install_packages;
&archive_packages;
&clean_up_temporary_files;
&distribute_files;
$build->complete('success', $logmsg);

sub common_prepare_code
{
    &collect_code;
    &set_code_version;
}

sub collect_code
{
    log_status('collect code');

    if (-d $modulesrcdir) {
	# Source exists, so update it
	unlink $modulesrcdir . '/configure.in'; # get rid of modified configure.in
	log_execution("cd $modulesrcdir && cvs up") or die $logmsg, "\n", 'cannot cvs up';
    } else {
	# No source present, so check it out
	log_execution("cd $topsrcdir && cvs -d$cvsroot co $modulename" or die $logmsg, "\ncannot cvs -d$cvsroot co $modulename");
    }

# FIXME: this sub should also identify changes, and exit(0) if there are
# none.  It analyze the output from cvs up unless a better way is
# devised.  If a better way is found, it should be moved into its own
# subroutine.
}

sub set_code_version
{
    my @configure_in_file;

    log_status('set code version');

    # Edit version in configure.in
    open CONFIGURE_IN, "$modulesrcdir/configure.in";
    @configure_in_file = <CONFIGURE_IN>;
    close CONFIGURE_IN;
    open CONFIGURE_IN, "> $modulesrcdir/configure.in";
    foreach (@configure_in_file) {
	if (!$packagename) {
	    s/^\s*AM_INIT_AUTOMAKE\s*\((.*?)\,\s*(.*?)([\,\)])/AM_INIT_AUTOMAKE\($1, $2cvs$datestr$3/;
	    $packagename = $1;
	    $packageversion = $2 . 'cvs' . $datestr;
	}
	print CONFIGURE_IN $_;
    }
    close CONFIGURE_IN;

}

sub prepare_package_system
{
    log_status('prepare package system');

    &uninstall_old_version;
}

sub uninstall_old_version
{
    log_status('--uninstall old version');
}

sub configure_code
{
    log_status('configure code');

    if (!(-d $modulebuilddir)) {
	mkdir $modulebuilddir, 0755;
    }

    log_execution("cd $modulebuilddir && $modulesrcdir/autogen.sh") or &build_failed;
}

sub customize_package_system
{
    log_status('customize package system');

    &make_dependent_on_installed_packages;
}

sub make_dependent_on_installed_packages
{
    log_status('--make dependent on installed packages');
}

sub prepare_code_archive
{
    log_status('prepare code archive');

    &make_dist;
    &place_in_correct_spot;
}

sub make_dist {
    log_status('--make dist');

    log_execution("cd $modulebuilddir && make dist") or &build_failed;
}

sub place_in_correct_spot {
    log_status('--place in correct spot');
}

sub prepare_system
{
    log_status('prepare system');

    &kill_processes;
}

sub kill_processes {
    log_status('--kill processes');
}

sub launch_package_build
{
    log_status('launch package build');

    log_execution("rpm -ta --define '_sysconfdir /gnome2-cvs/etc' --define '_prefix /gnome2-cvs/usr' $modulebuilddir/$packagename-$packageversion.tar.gz") or &build_failed;
}

sub install_packages
{
    log_status('install packages');

    log_execution("rpm -U $RPMDIR/RPMS/i386/$packagename-$packageversion*") or &build_failed('rpm cannot install package');
}

sub archive_packages
{
    log_status('archive packages');
}

sub clean_up_temporary_files
{
    log_status('clean up temporary files');
}

sub distribute_files
{
    log_status('distribute files');
}


sub log_execution
{
    local (*E_IN, *E_OUT);
    my ($childpid, @output);

    $childpid = open3(*E_IN, *E_OUT, *E_OUT, "sh -c \"$_[0]\"");
    close E_IN;
    @output = <E_OUT>;
    close E_OUT;
    waitpid($childpid, 0);
    if ($?) {$logmsg .= "build-script: wait status: $?\n"}
    $logmsg .= join('', @output);

    return !($? >> 8);
}

sub build_failed
{
    $build->complete('build_failed', $logmsg);
    die 'build failed';
}

sub log_status
{
    print $_[0], "\n";
}
