package GnomeTinderboxBuild;

use strict;

sub send_status
{
    my $MAILER = '/usr/lib/sendmail -t';
    my $MAILFROM = 'garrison@olga.net';
    my $ADMIN = 'garrison@olga.net';
#gnome-build-status@gnome.org

    my $timenow = time();
    my $self = shift;
    my $logmsg = shift;
    $logmsg ||= '';

    my $othercmds = '';
    foreach (keys %{$self}) {
	my $val = $self->{$_};
	$othercmds .= "tinderbox: $_: $val\n";
    }

    my ($message) = <<"EOF";
From: $MAILFROM
To: garrison\@olga.net
Subject: Tinderbox Build Update


tinderbox: tree: gnome
tinderbox: timenow $timenow
tinderbox: errorparser: unix
tinderbox: administrator: $ADMIN
$othercmds


$logmsg


EOF

    $message =~ s/([^\n]{1000})/$1\n/g;  
    $message =~ s/^\.$/\.\./mg;
    
    my ($pid) = open(MAIL, "|-");
    defined ($pid) || 
	die("Could not fork for cmd: '$MAILER': $!\n");
    ($pid) ||
	exec($MAILER) || 
	    die("Could not exec: '$MAILER': $!\n");
    print MAIL $message;
    close(MAIL) or
	($?) or
	    die("Could not close '$MAILER': $!\n");
    ($?) &&
	die("Could not close '$MAILER' wait_status: $? : $!\n");
}

sub start
{
    my $self = {
	'buildname'=>$_[1],
	'starttime'=>time(),
	'status'=>'building'
	};
    bless $self;

    send_status($self);

    return $self;
}

sub complete
{
    my $self = shift;

    if ($self->{'status'} ne 'building') {
	die 'build already completed';
    }

    $self->{'status'} = shift;

    send_status($self, shift); # Need logfiles too
}

1
